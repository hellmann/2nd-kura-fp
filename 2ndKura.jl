using LightGraphs
using OrdinaryDiffEq
using NLsolve
using Plots
using Sundials

N1 = 50
N = 100

function make_rd()
    g = barabasi_albert(N1, 4)
    for i in N1+1:N
        add_vertex!(g)
        add_edge!(g, i, rand(1:i-1))
    end
    g
end

g = make_rd()

B = incidence_matrix(g, oriented=true)

struct kuramoto_dyn{T, T2, U}
    B::T
    B_trans::T2
    P::U
    N::Int
    alpha::Float64
    K
end
function (dd::kuramoto_dyn)(dx, x, p, t)
    dx[1:N] .= dd.P .- dd.alpha .* x[1:N] .- dd.B * (dd.K .* sin.(dd.B_trans * x[N+1:2N]))
    dx[N+1:2N] .= x[1:N]
    nothing
end
function (dd::kuramoto_dyn)(dx, x)
    dx .= dd.P .- dd.B * (dd.K .* sin.(dd.B_trans * x))
    nothing
end

P = randn(N)
P .-= sum(P)/N

kn = kuramoto_dyn(B, transpose(B), P, N, 0.1, 8.)
# Jv = JacVecOperator(kn, randn(nv(g)), nothing, 0.0)
kur_net = ODEFunction(kn) # jac_prototype=Jv)

tspan = (0., 100.)
prob = ODEProblem(kur_net, randn(2N), tspan, nothing)

println("Solving")
sol = solve(prob, Rodas4(), abstol=10^-14)
ptspan = (90., 100.)
plot(sol, vars=collect(1:N), tspan=ptspan)

function calc_bs_vec(g, P, N_samp, pert_size = 1.)
    n_conv = zeros(Int, nv(g))
    B = incidence_matrix(g, oriented=true)
    kn = kuramoto_dyn(B, transpose(B), P, N, 0.1, 8.)
    kur_net = ODEFunction(kn)
    fp_res = nlsolve(kn, zeros(N))
    if ! converged(fp_res)
        println("No FP found")
        return nothing
    end

    x0 = vcat(zeros(nv(g)), fp_res.zero)

    for n in 1:nv(g)
        println("Working on node $n")
        for i in 1:N_samp
            x_pert = copy(x0)
            x_pert[[n, nv(g)+n]] .+= pert_size .* randn(2)
            prob = ODEProblem(kur_net, x_pert, tspan, nothing)
            sol = solve(prob, Tsit5(), abstol=10^-14)
            if all(abs.(sol[end][1:N]) .< 0.1)
                n_conv[n] += 1
            end
        end
    end
    n_conv ./ N_samp
end

bs = calc_bs_vec(g, P, 5, 20.)


for i in 1:10
    fp_found = false
    tr_conv = false
    g = make_rd()
    B = incidence_matrix(g, oriented=true)

    P = randn(N)
    P .-= sum(P)/N

    kn = kuramoto_dyn(B, transpose(B), P, N, 0.1, 8.)
    kur_net = ODEFunction(kn)

    fp_res = nlsolve(kn, zeros(N))
    fp_found = converged(fp_res)
    x0 = vcat(zeros(N), fp_res.zero)
    prob = ODEProblem(kur_net, x0 .+ 0.01 .* randn(2N), tspan, nothing)
    sol = solve(prob, Tsit5(), abstol=10^-14)
    # ptspan = (90., 100.)
    # plot(sol, vars=collect(1:10), tspan=ptspan)
    tr_conv = all(abs.(sol[end][1:N]) .< 0.1)
    println("Run $i: FP found $fp_found, Trajectory converged $tr_conv")
end

begin
    fp_found = false
    tr_conv = false
    g = make_rd()
    B = incidence_matrix(g, oriented=true)

    P = randn(N)
    P .-= sum(P)/N

    kn = kuramoto_dyn(B, transpose(B), P, N, 0.1, 8.)
    kur_net = ODEFunction(kn)

    fp_res = nlsolve(kn, zeros(N))
    fp_found = converged(fp_res)
    x0 = vcat(zeros(N), fp_res.zero)
    prob = ODEProblem(kur_net, x0 .+ 0.1 .* randn(2N), tspan, nothing)
    sol = solve(prob, CVODE_BDF(), abstol=10^-14)
    # ptspan = (90., 100.)
    # plot(sol, vars=collect(1:10), tspan=ptspan)
    tr_conv = all(abs.(sol[end][1:N]) .< 0.1)
    println("FP found $fp_found, Trajectory converged $tr_conv")

    plot(sol, vars=collect(1:N))
end
